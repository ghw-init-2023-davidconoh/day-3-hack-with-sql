from flask import Flask, jsonify, request, abort
from models import setup_db, Fruit
from flask_cors import CORS, cross_origin

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    setup_db(app)
    CORS(app)

    @app.after_request
    def after_request(response):
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
        response.headers.add('Access-Control-Allow-Headers', 'GET, POST, PATCH, DELETE, OPTIONS')
        return response

    #@cross_origin
    @app.route('/fruits', methods=['GET'])
    def get_fruits():
        page = request.args.get('page', 1, type=int)
        start = (page - 1) * 4
        end = start + 4
        fruits = Fruit.query.all()
        formatted_fruits = [fruit.format() for fruit in fruits]

        return jsonify({
            'success': True,
            'fruits': formatted_fruits[start:end],
            'total_fruits': len(formatted_fruits)
        })

    @app.route('/fruits/<int:fruit_id>')
    def get_specific_fruit(fruit_id):
        fruit = Fruit.query.filter(Fruit.id==fruit_id).one_or_none()
        if fruit is None:
            abort(404)
        else:
            return jsonify({
                'success': True,
                'fruit': fruit.format()
            })

    return app