# Day-3-Hack-with-SQL

GHW 2023 — Day 3: Hack with SQL Challenge — A concise example of using SQL to query data from the database and expose it in the backend using Flask, Postgres, SQLAlchemy, and shell.

## Notice
This application is built and tested on [Linux Mint](https://linuxmint.com) and would work well on other Ubuntu variants. You are welcome to try it on any other platform, however.

## Introduction
Before getting started, make sure you have the following set of applications setup on your prefered operating system: 
- Postres [Installation guide for Ubuntu](https://www.postgresql.org/download/linux/ubuntu/)
- Bash Shell environment: [Terminal](https://gitlab.gnome.org/GNOME/gnome-terminal) or [Konsole](https://konsole.kde.org/)
- Web Browser: [Chrome](https://www.google.com/chrome/) or [Firefox](https://www.mozilla.org/en-US/firefox/new/)

## Gettig started
To get started with this project, run the following commands in `bash`:

- Setup folder:
    ```
    mkdir ghw-init-2023
    cd ghw-init-2023
    ```
- Setup environment
    ```
    python3 -m venv sqlvenv
    source sqlvenv/bin/activate
    ```
- Get the repository files:
    ```
    git clone https://gitlab.com/ghw-init-2023-davidconoh/day-3-hack-with-sql.git
    cd day-3-hack-with-sql
    ```
- Install dependencies
    ```
    pip install -r requirements.txt --upgrade
    ```
- Setup database:
    ```
    psql -l # List all existing databases
    psql < ./setup/fruits.sql # Create database, and user 'gwh' with previledges
    psql fruits < ./setup/fruits.psql # Create tables and apply constraints
    ```
- Run application:
    ```
    export FLASK_APP=app
    export FLASK_ENV=development
    flask run
    ```

## Development
- To view the dataset in the browser, see [page 1](http://localhost:5000/fruits), [page 2](http://localhost:5000/fruits?page=2), and [page 3](http://localhost:5000/fruits?page=3)

## Test
This project uses the [Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/) for testing.

## Description
This simple web application backend is designed to showcase the power of SQL for database management. At an abstracted layer, we use Flask-SQLAlchemy to simplify reading from and writing to our database. This is a good project for a beginner-level developer and simplified to showcase the power of a hack!

— Global Hack Week: INIT (2023) — Day 3

## Author
- Chika Onoh [@davidconoh](https://gitlab.com/davidconoh)

## Acknowledgment
This project is dedicated to the Major League Hacking Team for organzing a one-of-a-kind Hackathon to kickstart the [MLH Hacking Season 2023](https://ghw.mlh.io/init). Also, appreciation goes all hackers hosting amazing hackathons this season.

Best!

## License
This project is licensed under the [MIT License](https://gitlab.com/ghw-init-2023-davidconoh/day-3-hack-with-sql/-/blob/main/LICENSE).
